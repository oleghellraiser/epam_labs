﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.WebUI.ViewModels
{
    public class TakenTicketViewModel
    {
        public string BuyerInfo { get; set; }
        public Ticket Ticket { get; set; }

        public TakenTicketViewModel(string buyerInfo, Ticket ticket)
        {
            BuyerInfo = buyerInfo;
            Ticket = ticket;
        }
    }
}
