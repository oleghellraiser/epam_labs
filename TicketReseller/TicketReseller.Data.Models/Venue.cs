﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketReseller.Data.Models
{
    public class Venue : BaseEntity
    {
        [Required(ErrorMessage = "Please enter Venue name!")]
        [StringLength(25, ErrorMessage = "Venue name cannot be longer than 25 and shorter than 3 characters.", MinimumLength = 3)]
        [Display(Name = "Name : ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter Venue address!")]
        [Display(Name = "Address : ")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please enter City!")]
        [Display(Name = "City : ")]
        public int? CityId { get; set; }
        public City City { get; set; }
        public ICollection<Perfomance> Perfomances { get; set; }
    }
}
