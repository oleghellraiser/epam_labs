﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.API.Controllers
{
    [Area("API")]
    [Route("[Area]/[controller]")]
    public class RelevantResultsController : Controller
    {
        private UnitOfWork _unitOfWork;
        private const int CountTopResults = 10;

        public RelevantResultsController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get([FromQuery]string searchString = "")
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                var topResults = _unitOfWork.PerfomanceRepository.Get(i => i.Name.ToLower().Contains(searchString.ToLower()))
                    .Select(i => i.Name).ToList();

                return Ok(topResults);
            }
            else
            {
                return NoContent();

            }
        }
    }
}
