﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using TicketReseller.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class PerfomanceManagementController : Controller
    {
        private UnitOfWork _unitOfWork;
        private IHostingEnvironment _appEnvironment;
        private readonly IStringLocalizer<PerfomanceManagementController> localizer;

        public PerfomanceManagementController(IStringLocalizer<PerfomanceManagementController> localizer, UnitOfWork unitOfWork, IHostingEnvironment appEnvironment)
        {
            _unitOfWork = unitOfWork;
            _appEnvironment = appEnvironment;
            this.localizer = localizer;

        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var perfomances = _unitOfWork.PerfomanceRepository.Get();
            return View(perfomances);
        }

        public ActionResult Create()
        {
            PopulateVenuesDropDownList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Perfomance perfomance, IFormFile uploadedFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadedFile != null)
                    {
                        string path = "/Images/" + uploadedFile.FileName;
                        using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }

                        perfomance.Image = path;
                        _unitOfWork.PerfomanceRepository.Insert(perfomance);
                        _unitOfWork.Save();
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }

            PopulateVenuesDropDownList();
            return View(perfomance);
        }

        public ActionResult Edit(int id)
        {
            var perfomance = _unitOfWork.PerfomanceRepository.GetByID(id);

            if (perfomance == null)
            {
                return RedirectToAction("Index");
            }

            PopulateVenuesDropDownList(perfomance.VenueId);

            return View(perfomance);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Perfomance perfomance, IFormFile uploadedFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadedFile != null)
                    {
                        string path = "/Images/" + uploadedFile.FileName;
                        using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }

                        perfomance.Image = path;
                    }
                    _unitOfWork.PerfomanceRepository.Update(perfomance);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }

            PopulateVenuesDropDownList(perfomance.VenueId);
            return View(perfomance);
        }

        private void PopulateVenuesDropDownList(object selectedVenue = null)
        {
            var venues = _unitOfWork.VenueRepository.Get(orderBy: q => q.OrderBy(d => d.Name));
            ViewBag.Venues = new SelectList(venues, "Id", "Name", selectedVenue);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var perfomance = _unitOfWork.PerfomanceRepository.GetByID(id);

                if (perfomance == null)
                {
                    return RedirectToAction("Index");
                }
                _unitOfWork.PerfomanceRepository.Delete(id);
                _unitOfWork.Save();
            }
            catch (DbUpdateException /* dex */)
            {
                ViewData["ErrorMessage"] =
                    localizer["Delete failed. Try again, and if the problem persists " +
                    "see your system administrator."];
            }
            return RedirectToAction("Index");
        }

    }
}
