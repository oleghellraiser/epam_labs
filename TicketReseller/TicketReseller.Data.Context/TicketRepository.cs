﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class TicketRepository : GenericRepository<Ticket>
    {
        public TicketRepository(DataContext context) : base(context)
        {
        }

        public override List<Ticket> Get(Expression<Func<Ticket, bool>> filter = null, Func<IQueryable<Ticket>,
            IOrderedQueryable<Ticket>> orderBy = null, Expression<Func<Ticket, object>>[] includeProperties = null)
        {
            var tickets = _context.Set<Ticket>()
                .Include(t => t.User)
                .Include(t => t.Perfomance)
                .AsQueryable();

            if (filter != null)
            {
                tickets = tickets.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(tickets).ToList();
            }
            return tickets.ToList();
        }

        public override Ticket GetByID(int id)
        {
            var ticket = _context.Set<Ticket>()
                .Include(t => t.Perfomance)
                .FirstOrDefault(c => c.Id == id);
            return ticket;
        }
    }
}
