﻿using System.ComponentModel.DataAnnotations;


namespace TicketReseller.WebUI.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Input your Email.")]
        [Display(Name = "E-mail: ")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Input your password.")]
        [Display(Name = "Password: ")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
