﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.WebUI.Areas.API.ModelDTO
{
    public class VenueDTO : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public City City { get; set; }
    }
}
