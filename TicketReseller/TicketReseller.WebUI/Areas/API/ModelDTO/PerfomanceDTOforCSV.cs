﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.WebUI.Areas.API.ModelDTO
{
    public class PerfomanceDTOforCSV : BaseEntity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Date { get; set; }
        public string Venue { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public int Availible { get; set; }
        public string Description { get; set; }
    }
}
