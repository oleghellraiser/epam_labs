﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicketReseller.Data.Models
{
    public class City : BaseEntity
    {
        [Required(ErrorMessage = "Please enter City name!")]
        [StringLength(25, ErrorMessage = "City name cannot be longer than 25 and shorter than 3 characters.", MinimumLength = 3)]
        [Display(Name = "Name : ")]
        public string Name { get; set; }
    }
}
