﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicketReseller.Data.Models
{
    public enum UserLocalization
    {
        En,
        Ru,
        Be
    };
}
