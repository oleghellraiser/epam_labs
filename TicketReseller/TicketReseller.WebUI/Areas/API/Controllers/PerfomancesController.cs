﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using AutoMapper;
using TicketReseller.WebUI.Areas.API.ModelDTO;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.API.Controllers
{
    [Area("API")]
    [Route("[Area]/[controller]")]
    public class PerfomancesController : Controller
    {
        private UnitOfWork _unitOfWork;
        private IMapper _mapper;
        private const int PageSize = 6;

        public PerfomancesController(UnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IActionResult Get([FromHeader]string Accept,
            [FromQuery]DateTime fromDate, [FromQuery]DateTime toDate,
            [FromQuery]List<int> cityIndexes, [FromQuery]List<int> venueIndexes,
            [FromQuery]int offset = 0, [FromQuery]int limit = PageSize,
            [FromQuery]string orderBy = "", [FromQuery]string eventName = "")
        {
            var perfomances = _unitOfWork.PerfomanceRepository.Get(fromDate, toDate, cityIndexes, venueIndexes,
                    orderBy, eventName);

            if (perfomances.Count != 0)
            {
                HttpContext.Response.Headers.Add("Total-items", perfomances.Count.ToString());
                if (limit != 0)
                    perfomances = perfomances.Skip(offset).Take(limit).ToList();
            }
            else
                return NoContent();


            if (Accept.Contains("application/csv"))
            {
                return Ok(_mapper.Map<List<PerfomanceDTOforCSV>>(perfomances));
            }
            else
            {
                return Ok(_mapper.Map<List<PerfomanceDTO>>(perfomances));
            }
        }
    }
}
