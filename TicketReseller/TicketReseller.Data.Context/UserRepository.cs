﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class UserRepository : GenericRepository<User>
    {
        public UserRepository(DataContext context) : base(context)
        {
        }

        public User GetUserByEmail(string mail)
        {
            return base.Get().FirstOrDefault(i => i.Email == mail);
        }
    }
}
