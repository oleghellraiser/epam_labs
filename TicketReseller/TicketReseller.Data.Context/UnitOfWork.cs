﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class UnitOfWork
    {
        private PerfomanceRepository perfomanceRepository;
        private TicketRepository ticketRepository;
        private CityRepository cityRepository;
        private UserRepository userRepository;
        private VenueRepository venueRepository;
        private OrderRepository orderRepository;

        private DataContext context;

        private static object syncRoot = new Object();

        public UnitOfWork(DataContext context)
        {
            this.context = context;
        }

        public PerfomanceRepository PerfomanceRepository
        {
            get
            {
                if (this.perfomanceRepository == null)
                {
                    lock (syncRoot)
                    {
                        if (this.perfomanceRepository == null)
                            this.perfomanceRepository = new PerfomanceRepository(context);
                    }
                }
                return perfomanceRepository;
            }
        }

        public TicketRepository TicketRepository
        {
            get
            {
                if (this.ticketRepository == null)
                {
                    lock (syncRoot)
                    {
                        if (this.ticketRepository == null)
                            this.ticketRepository = new TicketRepository(context);
                    }
                }
                return ticketRepository;
            }
        }

        public CityRepository CityRepository
        {
            get
            {
                if (this.cityRepository == null)
                {
                    lock (syncRoot)
                    {
                        if (this.cityRepository == null)
                            this.cityRepository = new CityRepository(context);
                    }
                }
                return cityRepository;
            }
        }

        public VenueRepository VenueRepository
        {
            get
            {
                if (this.venueRepository == null)
                {
                    lock (syncRoot)
                    {
                        if (this.venueRepository == null)
                            this.venueRepository = new VenueRepository(context);
                    }
                }
                return venueRepository;
            }
        }

        public UserRepository UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    lock (syncRoot)
                    {
                        if (this.userRepository == null)
                            this.userRepository = new UserRepository(context);
                    }
                }
                return userRepository;
            }
        }

        public OrderRepository OrderRepository
        {
            get
            {
                if (this.orderRepository == null)
                {
                    lock (syncRoot)
                    {
                        if (this.orderRepository == null)
                            this.orderRepository = new OrderRepository(context);
                    }
                }
                return orderRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }


    }
}