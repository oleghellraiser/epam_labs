﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;

namespace TicketReseller.WebUI.Infrastructure
{
    public class HandlingFormat
    {
        public static IFormatProvider GetFormatDate()
        {
            DateTimeFormatInfo dateFormat = new DateTimeFormatInfo();
            dateFormat.ShortDatePattern = "dd.MM.yy";
            return dateFormat;
        }

        public static IFormatProvider GetFormatCurrency()
        {
            NumberFormatInfo nf = new NumberFormatInfo();
            nf.CurrencySymbol = "$";
            return nf;
        }
    }
}
