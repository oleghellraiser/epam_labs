﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.WebUI.ViewModels;
using TicketReseller.Data.Context;
using Microsoft.AspNetCore.Http;
using TicketReseller.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace TicketReseller.WebUI.Areas.Main.Controllers
{
    [Area("Main")]
    public class HomeController : Controller
    {
        private UnitOfWork _unitOfWork;
        public int PageSize = 6;

        public HomeController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewResult Index(int page = 1, int? filterValue = null)
        {
            List<Perfomance> perfomances;
            if (filterValue == null)
                perfomances = _unitOfWork.PerfomanceRepository.Get();
            else
                perfomances = _unitOfWork.PerfomanceRepository.Get(p => p.Venue.CityId == filterValue);
                       
            var citiesFilter = _unitOfWork.CityRepository.GetCitiesByPerfomances(perfomances);
            ViewBag.Cities = citiesFilter;

            var perfomanceList = new PerfomanceListViewModel
            {
                Perfomances = perfomances
                .OrderBy(p => p.Name)
                .Skip((page - 1) * PageSize)
                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = perfomances.Count()
                }
            };
            return View(perfomanceList);
        }

        public ActionResult Details(int id)
        {
            var perfomance = _unitOfWork.PerfomanceRepository.GetByID(id);
            var tickets = _unitOfWork.TicketRepository.Get(i => i.PerfomanceId == id &&
                i.StatusId == TicketStatus.Selling, i => i.OrderBy(t => t.Price));

            if (perfomance != null)
            {
                return View(new TicketListViewModel(perfomance, tickets));
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult SelectLanguage(string cultureValue)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                User user = _unitOfWork.UserRepository.GetUserByEmail(HttpContext.User.Identity.Name);

                user.LocalizationId = (UserLocalization)Enum.Parse(typeof(UserLocalization), cultureValue);
                _unitOfWork.UserRepository.Update(user);
            }
            RouteData.Values["culture"] = cultureValue.ToLower();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
