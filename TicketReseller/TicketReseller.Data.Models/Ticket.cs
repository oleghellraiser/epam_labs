﻿using System.ComponentModel.DataAnnotations;

namespace TicketReseller.Data.Models
{
    public class Ticket : BaseEntity
    {
        [Required(ErrorMessage = "Please enter Ticket price!")]
        [Display(Name = "Price : ")]
        public float Price { get; set; }

        [Required(ErrorMessage = "Please enter Ticket note!")]
        [Display(Name = "Notes : ")]
        public string SellerNotes { get; set; }

        [Required(ErrorMessage = "Please enter Perfomance!")]
        [Display(Name = "Perfomance : ")]
        public int? PerfomanceId { get; set; }
        public Perfomance Perfomance { get; set; }

        public TicketStatus StatusId { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
    }
}
