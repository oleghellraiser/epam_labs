﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using TicketReseller.Data.Models;
using TicketReseller.WebUI.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using TicketReseller.Data.Context;
using Microsoft.Extensions.Localization;
using TicketReseller.WebUI.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using TicketReseller.WebUI.Areas.Admin.ViewModels;

namespace TicketReseller.WebUI.Areas.Main.Controllers
{
    [Area("Main")]
    public class AccountController : Controller
    {
        private UnitOfWork _unitOfWork;
        private readonly IStringLocalizer<AccountController> localizer;

        public AccountController(IStringLocalizer<AccountController> localizer, UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            this.localizer = localizer;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _unitOfWork.UserRepository.GetUserByEmail(model.Email);
                if (user == null)
                {
                    user = new User
                    {
                        Email = model.Email,
                        Password = SecurityHelper.Hash(model.Password),
                        RoleId = UserRole.User
                    };

                    _unitOfWork.UserRepository.Insert(user);
                    _unitOfWork.Save();

                    await Authenticate(user);

                    return RedirectToAction("UserInfo", "Account");
                }
                else
                    ModelState.AddModelError("", localizer["Your Login has already existed."]);
            }
            return View(model);
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _unitOfWork.UserRepository.Get()
                    .FirstOrDefault(u => u.Email == model.Email && u.Password == SecurityHelper.Hash(model.Password));
                if (user != null)
                {
                    await Authenticate(user);
                    RouteData.Values["culture"] = user.LocalizationId.ToString().ToLower();

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", localizer["Invalid Login attempt."]);
            }
            return View(model);
        }
        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.RoleId.ToString())
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        [HttpGet]
        [Authorize]
        public IActionResult UserInfo()
        {
            User user = _unitOfWork.UserRepository.GetUserByEmail(HttpContext.User.Identity.Name);
            return View(user);
        }

        [HttpPost]
        [Authorize]
        public IActionResult UserInfo(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _unitOfWork.UserRepository.GetUserByEmail(HttpContext.User.Identity.Name);
                    user.Address = userViewModel.Address;
                    user.FirstName = userViewModel.FirstName;
                    user.LastName = userViewModel.LastName;
                    user.PhoneNumber = userViewModel.PhoneNumber;

                    _unitOfWork.UserRepository.Update(user);
                    _unitOfWork.Save();
                    return RedirectToAction("Index","Home");
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }
            return View(userViewModel);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            RouteData.Values["culture"] = CustomRouteCultureProvider.GetDefaultCulture;

            return RedirectToAction("Index", "Home");
        }

    }
}