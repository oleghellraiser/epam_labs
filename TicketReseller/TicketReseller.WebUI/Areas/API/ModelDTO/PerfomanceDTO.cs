﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.WebUI.Areas.API.ModelDTO
{
    public class PerfomanceDTO : BaseEntity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Date { get; set; }
        public int? VenueId { get; set; }
        public VenueDTO Venue { get; set; }
        public string Description { get; set; }
        public int Availible { get; set; }
    }
}
