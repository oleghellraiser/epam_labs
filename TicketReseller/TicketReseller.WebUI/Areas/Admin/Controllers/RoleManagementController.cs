﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using TicketReseller.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using TicketReseller.WebUI.Areas.Admin.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class RoleManagementController : Controller
    {
        private UnitOfWork _unitOfWork;
        private IMapper _mapper;
        private readonly IStringLocalizer<RoleManagementController> localizer;

        public RoleManagementController(IStringLocalizer<RoleManagementController> localizer,
            IMapper mapper, UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            this.localizer = localizer;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View(_mapper.Map<List<UserViewModel>>(_unitOfWork.UserRepository.Get()));
        }

        public ActionResult Edit(int id)
        {
            var user = _unitOfWork.UserRepository.GetByID(id);

            if (user == null)
            {
                return RedirectToAction("Index");
            }
            return View(_mapper.Map<UserViewModel>(user));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = _unitOfWork.UserRepository.GetByID(userViewModel.Id);
                    user.RoleId = userViewModel.RoleId;
                    _unitOfWork.UserRepository.Update(user);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }
            return View(userViewModel);
        }
    }
}
