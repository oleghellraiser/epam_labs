﻿using System.Collections.Generic;
using TicketReseller.Data.Models;

namespace TicketReseller.WebUI.ViewModels
{
    public class TicketListViewModel
    {
        public Perfomance Perfomance { get; set; }
        public IEnumerable<Ticket> TicketList { get; set; }

        public TicketListViewModel(Perfomance perfomance, IEnumerable<Ticket> ticketList)
        {
            Perfomance = perfomance;
            TicketList = ticketList;
        }
    }
}
