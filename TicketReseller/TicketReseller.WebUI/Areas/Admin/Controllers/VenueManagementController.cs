﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using TicketReseller.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class VenueManagementController : Controller
    {
        private UnitOfWork _unitOfWork;
        private readonly IStringLocalizer<VenueManagementController> localizer;

        public VenueManagementController(IStringLocalizer<VenueManagementController> localizer, UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            this.localizer = localizer;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var venues = _unitOfWork.VenueRepository.Get();
            return View(venues);
        }

        public ActionResult Create()
        {
            PopulateCitiesDropDownList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Venue venue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.VenueRepository.Insert(venue);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }
            return View(venue);
        }

        public ActionResult Edit(int id)
        {
            var venue = _unitOfWork.VenueRepository.GetByID(id);

            if (venue == null)
            {
                return RedirectToAction("Index");
            }

            PopulateCitiesDropDownList(venue.CityId);

            return View(venue);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Venue venue)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.VenueRepository.Update(venue);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }
            return View(venue);
        }

        private void PopulateCitiesDropDownList(object selectedCity = null)
        {
            var cities = _unitOfWork.CityRepository.Get(orderBy: q => q.OrderBy(d => d.Name));
            ViewBag.Cities = new SelectList(cities, "Id", "Name", selectedCity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var venue = _unitOfWork.VenueRepository.GetByID(id);

                if (venue == null)
                {
                    return RedirectToAction("Index");
                }
                _unitOfWork.VenueRepository.Delete(id);
                _unitOfWork.Save();
            }
            catch (DbUpdateException /* dex */)
            {
                ViewData["ErrorMessage"] =
                    localizer["Delete failed. Try again, and if the problem persists " +
                    "see your system administrator."];
            }
            return RedirectToAction("Index");
        }

    }
}
