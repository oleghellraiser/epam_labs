﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using TicketReseller.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CityManagementController : Controller
    {
        private UnitOfWork _unitOfWork;
        private readonly IStringLocalizer<CityManagementController> localizer;


        public CityManagementController(IStringLocalizer<CityManagementController> localizer, UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            this.localizer = localizer;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var cities = _unitOfWork.CityRepository.Get();
            return View(cities);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(City city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.CityRepository.Insert(city);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }
            return View(city);
        }

        public ActionResult Edit(int id)
        {
            var city = _unitOfWork.CityRepository.GetByID(id);

            if (city == null)
            {
                return RedirectToAction("Index");
            }
            return View(city);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(City city)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _unitOfWork.CityRepository.Update(city);
                    _unitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }
            return View(city);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                var city = _unitOfWork.CityRepository.GetByID(id);

                if (city == null)
                {
                    return RedirectToAction("Index");
                }
                _unitOfWork.CityRepository.Delete(id);
                _unitOfWork.Save();
            }
            catch (DbUpdateException /* dex */)
            {
                ViewData["ErrorMessage"] =
                    localizer["Delete failed. Try again, and if the problem persists " +
                    "see your system administrator."];
            }
            return RedirectToAction("Index");
        }
    }
}
