﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class CityRepository : GenericRepository<City>
    {
        public CityRepository(DataContext context) : base(context)
        {
        }

        public List<City> GetCitiesByPerfomances(List<Perfomance> perfomances)
        {
            List<City> citiesFilter = new List<City>();
            var cities = _context.Set<City>().ToList();
            try
            {
                cities.ForEach(i => citiesFilter.Add(perfomances
                    .FirstOrDefault(p => p.Venue.CityId == i.Id).Venue.City));
            }
            catch (NullReferenceException /*e*/)
            {
                return cities;
            }
            return citiesFilter;
        }
    }
}
