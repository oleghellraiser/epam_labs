﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using TicketReseller.Data.Models;
using System;
using Microsoft.EntityFrameworkCore;

namespace TicketReseller.Data.Context
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            DataContext context = app.ApplicationServices
                .GetRequiredService<DataContext>();
            if (!context.Cities.Any())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        AddCities(context);
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Cities] ON");

                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Cities] OFF");
                    }
                    catch (InvalidOperationException /*e*/)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            if (!context.Venues.Any())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        AddVenues(context);
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Venues] ON");

                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Venues] OFF");
                    }
                    catch (InvalidOperationException /*e*/)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            if (!context.Perfomances.Any())
            {
                
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        AddPerfomaces(context);
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Perfomances] ON");

                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Perfomances] OFF");
                    }
                    catch (InvalidOperationException /*e*/)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }

            if (!context.Users.Any())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        AddUsers(context);
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Users] ON");

                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Users] OFF");
                    }
                    catch (InvalidOperationException /*e*/)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }

            if (!context.Tickets.Any())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        AddTickets(context);
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Tickets] ON");

                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Tickets] OFF");
                    }
                    catch (InvalidOperationException /*e*/)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }
            if (!context.Orders.Any())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        AddOrders(context);
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Orders] ON");

                        context.SaveChanges();
                        dbContextTransaction.Commit();
                        context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Orders] OFF");
                    }
                    catch (InvalidOperationException /*e*/)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            }

        }

        private static void AddPerfomaces(DataContext context)
        {
            context.Perfomances.AddRange(
                new Perfomance
                {
                    Id = 1,
                    Name = "Perfomance item 1",
                    Description = "Here is some text",
                    VenueId = 1,
                    Date = new DateTime(2016, 12, 10),
                    Image = "/images/events-heavenly-header.jpg"
                },
                new Perfomance
                {
                    Id = 2,
                    Name = "Perfomance item 2",
                    Description = "Here is some text",
                    VenueId = 2,
                    Date = new DateTime(2016, 12, 12),
                    Image = "/images/events-heavenly-header.jpg"
                }, new Perfomance
                {
                    Id = 3,
                    Name = "Perfomance item 3",
                    Description = "Here is some text",
                    VenueId = 3,
                    Date = new DateTime(2016, 12, 11),
                    Image = "/images/events-heavenly-header.jpg"
                }, new Perfomance
                {
                    Id = 4,
                    Name = "Perfomance item 4",
                    Description = "Here is some text",
                    VenueId = 1,
                    Date = new DateTime(2016, 12, 13),
                    Image = "/images/events-heavenly-header.jpg"
                }, new Perfomance
                {
                    Id = 5,
                    Name = "Perfomance item 5",
                    Description = "Here is some text",
                    VenueId = 2,
                    Date = new DateTime(2016, 12, 14),
                    Image = "/images/events-heavenly-header.jpg"
                }, new Perfomance
                {
                    Id = 6,
                    Name = "Perfomance item 6",
                    Description = "Here is some text",
                    VenueId = 3,
                    Date = new DateTime(2016, 12, 15),
                    Image = "/images/events-heavenly-header.jpg"
                }, new Perfomance
                {
                    Id = 7,
                    Name = "Perfomance item 7",
                    Description = "Here is some text",
                    VenueId = 1,
                    Date = new DateTime(2016, 12, 16),
                    Image = "/images/events-heavenly-header.jpg"
                }, new Perfomance
                {
                    Id = 8,
                    Name = "Perfomance item 8",
                    Description = "Here is some text",
                    VenueId = 2,
                    Date = new DateTime(2016, 12, 17),
                    Image = "/images/events-heavenly-header.jpg"
                }, new Perfomance
                {
                    Id = 9,
                    Name = "Perfomance item 9",
                    Description = "Here is some text",
                    VenueId = 3,
                    Date = new DateTime(2016, 12, 18),
                    Image = "/images/events-heavenly-header.jpg"
                });

        }

        private static void AddTickets(DataContext context)
        {
            context.Tickets.AddRange(
                new Ticket
                {
                    Id = 1,
                    Price = 15,
                    SellerNotes = "Here is some text1",
                    PerfomanceId = 3,
                    UserId = 1,
                    StatusId = TicketStatus.Sold
                }, new Ticket
                {
                    Id = 2,
                    Price = 17,
                    SellerNotes = "Here is some text2",
                    PerfomanceId = 2,
                    UserId = 2,
                    StatusId = TicketStatus.Selling
                }, new Ticket
                {
                    Id = 3,
                    Price = 21,
                    SellerNotes = "Here is some text3",
                    PerfomanceId = 1,
                    UserId = 3,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 4,
                    Price = 18,
                    SellerNotes = "Here is some text4",
                    PerfomanceId = 3,
                    UserId = 2,
                    StatusId = TicketStatus.Sold
                }, new Ticket
                {
                    Id = 5,
                    Price = 20,
                    SellerNotes = "Here is some text5",
                    PerfomanceId = 2,
                    UserId = 3,
                    StatusId = TicketStatus.Selling
                }, new Ticket
                {
                    Id = 6,
                    Price = 22,
                    SellerNotes = "Here is some text6",
                    PerfomanceId = 1,
                    UserId = 1,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 7,
                    Price = 23,
                    SellerNotes = "Here is some text7",
                    PerfomanceId = 4,
                    UserId = 3,
                    StatusId = TicketStatus.Sold
                }, new Ticket
                {
                    Id = 8,
                    Price = 25,
                    SellerNotes = "Here is some text8",
                    PerfomanceId = 5,
                    UserId = 2,
                    StatusId = TicketStatus.Selling
                }, new Ticket
                {
                    Id = 9,
                    Price = 24,
                    SellerNotes = "Here is some text9",
                    PerfomanceId = 6,
                    UserId = 1,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 10,
                    Price = 26,
                    SellerNotes = "Here is some text10",
                    PerfomanceId = 4,
                    UserId = 2,
                    StatusId = TicketStatus.Sold
                }, new Ticket
                {
                    Id = 11,
                    Price = 17,
                    SellerNotes = "Here is some text11",
                    PerfomanceId = 5,
                    UserId = 3,
                    StatusId = TicketStatus.Selling
                }, new Ticket
                {
                    Id = 12,
                    Price = 21,
                    SellerNotes = "Here is some text12",
                    PerfomanceId = 6,
                    UserId = 1,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 13,
                    Price = 18,
                    SellerNotes = "Here is some text13",
                    PerfomanceId = 3,
                    UserId = 2,
                    StatusId = TicketStatus.Sold
                }, new Ticket
                {
                    Id = 14,
                    Price = 20,
                    SellerNotes = "Here is some text14",
                    PerfomanceId = 2,
                    UserId = 3,
                    StatusId = TicketStatus.Selling
                }, new Ticket
                {
                    Id = 15,
                    Price = 22,
                    SellerNotes = "Here is some text15",
                    PerfomanceId = 1,
                    UserId = 1,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 16,
                    Price = 16,
                    SellerNotes = "Here is some text16",
                    PerfomanceId = 3,
                    UserId = 3,
                    StatusId = TicketStatus.Sold
                }, new Ticket
                {
                    Id = 17,
                    Price = 17,
                    SellerNotes = "Here is some text17",
                    PerfomanceId = 2,
                    UserId = 2,
                    StatusId = TicketStatus.Selling
                }, new Ticket
                {
                    Id = 18,
                    Price = 28,
                    SellerNotes = "Here is some text18",
                    PerfomanceId = 1,
                    UserId = 2,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 19,
                    Price = 16,
                    SellerNotes = "Here is some text19",
                    PerfomanceId = 2,
                    UserId = 2,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 20,
                    Price = 17,
                    SellerNotes = "Here is some text20",
                    PerfomanceId = 3,
                    UserId = 2,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 21,
                    Price = 19,
                    SellerNotes = "Here is some text21",
                    PerfomanceId = 1,
                    UserId = 1,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 22,
                    Price = 22,
                    SellerNotes = "Here is some text22",
                    PerfomanceId = 1,
                    UserId = 1,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 23,
                    Price = 16,
                    SellerNotes = "Here is some text23",
                    PerfomanceId = 3,
                    UserId = 3,
                    StatusId = TicketStatus.Sold
                }, new Ticket
                {
                    Id = 24,
                    Price = 17,
                    SellerNotes = "Here is some text24",
                    PerfomanceId = 2,
                    UserId = 2,
                    StatusId = TicketStatus.Selling
                }, new Ticket
                {
                    Id = 25,
                    Price = 28,
                    SellerNotes = "Here is some text25",
                    PerfomanceId = 1,
                    UserId = 2,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 26,
                    Price = 16,
                    SellerNotes = "Here is some text26",
                    PerfomanceId = 2,
                    UserId = 2,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 27,
                    Price = 17,
                    SellerNotes = "Here is some text27",
                    PerfomanceId = 3,
                    UserId = 2,
                    StatusId = TicketStatus.WaitingConfirmation
                }, new Ticket
                {
                    Id = 28,
                    Price = 19,
                    SellerNotes = "Here is some text28",
                    PerfomanceId = 1,
                    UserId = 1,
                    StatusId = TicketStatus.WaitingConfirmation
                });

        }

        private static void AddUsers(DataContext context)
        {
            context.Users.AddRange(new User
            {
                Id = 1,
                Email = "user@user.com",
                FirstName = "Ivan",
                LastName = "Sidorov",
                LocalizationId = UserLocalization.Ru,
                Address = "Gomel, 14 Sovietskaya str.",
                PhoneNumber = "+375291231231",
                Password = "OomN4ApItLW8Koc9XoBBMgFOqBsd/GLbRtZkbHSP1A4=", //"user"
                RoleId = UserRole.User
            }, new User
            {
                Id = 2,
                Email = "admin@admin.com",
                FirstName = "Dolph",
                LastName = "Lundgren",
                LocalizationId = UserLocalization.En,
                Address = "Gomel, 17 Lenina str.",
                PhoneNumber = "+375299877899",
                Password = "A4guK/imGsZwvJz55IZwtFNJVp+w/LWzTrOmX8Wl4qY=", //"admin"
                RoleId = UserRole.Admin

            }, new User
            {
                Id = 3,
                Email = "stiven@user.com",
                FirstName = "Stiven",
                LastName = "Sigal",
                LocalizationId = UserLocalization.Be,
                Address = "Gomel, 23 Kirova str.",
                PhoneNumber = "+375294566544",
                Password = "OomN4ApItLW8Koc9XoBBMgFOqBsd/GLbRtZkbHSP1A4=", //"user"
                RoleId = UserRole.User
            });

        }

        private static void AddVenues(DataContext context)
        {
            context.Venues.AddRange(new Venue
            {
                Id = 1,
                Name = "Central Stadium",
                Address = "b13, Pobeda str.",
                CityId = 1
            }, new Venue
            {
                Id = 2,
                Name = "Minsk Arena",
                Address = "b33, Kyznecova str.",
                CityId = 2
            }, new Venue
            {
                Id = 3,
                Name = "Prime Hall",
                Address = "b25, Gorkogo str.",
                CityId = 3
            });
        }

        private static void AddCities(DataContext context)
        {
            context.Cities.AddRange(new City
            {
                Id = 1,
                Name = "Gomel",
            }, new City
            {
                Id = 2,
                Name = "Minsk",
            }, new City
            {
                Id = 3,
                Name = "Moscow",
            });

        }

        private static void AddOrders(DataContext context)
        {
            context.Orders.AddRange(new Order
            {
                Id = 1,
                TicketId = 1,
                TrackNum = "78984565EV1",
                StatusId = OrderStatus.Accepted,
                UserId = 1
            }, new Order
            {
                Id = 2,
                TicketId = 2,
                TrackNum = "78984565EV2",
                StatusId = OrderStatus.Declined,
                UserId = 2
            }, new Order
            {
                Id = 3,
                TicketId = 3,
                TrackNum = "78984565EV3",
                StatusId = OrderStatus.InProgress,
                UserId = 3
            }, new Order
            {
                Id = 4,
                TicketId = 4,
                TrackNum = "78984565EV4",
                StatusId = OrderStatus.Accepted,
                UserId = 3
            }, new Order
            {
                Id = 5,
                TicketId = 5,
                TrackNum = "78984565EV5",
                StatusId = OrderStatus.Declined,
                UserId = 2
            }, new Order
            {
                Id = 6,
                TicketId = 6,
                TrackNum = "78984565EV6",
                StatusId = OrderStatus.InProgress,
                UserId = 1
            }, new Order
            {
                Id = 7,
                TicketId = 7,
                TrackNum = "78984565EV7",
                StatusId = OrderStatus.Accepted,
                UserId = 3
            }, new Order
            {
                Id = 8,
                TicketId = 8,
                TrackNum = "78984565EV8",
                StatusId = OrderStatus.Declined,
                UserId = 2
            }, new Order
            {
                Id = 9,
                TicketId = 9,
                TrackNum = "78984565EV9",
                StatusId = OrderStatus.InProgress,
                UserId = 1
            }, new Order
            {
                Id = 10,
                TicketId = 10,
                TrackNum = "78984565EV10",
                StatusId = OrderStatus.Accepted,
                UserId = 3
            }, new Order
            {
                Id = 11,
                TicketId = 11,
                TrackNum = "78984565EV11",
                StatusId = OrderStatus.Declined,
                UserId = 2
            }, new Order
            {
                Id = 12,
                TicketId = 18,
                TrackNum = "4564ER45456",
                StatusId = OrderStatus.InProgress,
                UserId = 1
            }, new Order
            {
                Id = 13,
                TicketId = 19,
                TrackNum = "783265EQ231",
                StatusId = OrderStatus.InProgress,
                UserId = 3
            }, new Order
            {
                Id = 14,
                TicketId = 20,
                TrackNum = "126578OP785",
                StatusId = OrderStatus.InProgress,
                UserId = 1
            }, new Order
            {
                Id = 15,
                TicketId = 12,
                TrackNum = "78984565EV12",
                StatusId = OrderStatus.InProgress,
                UserId = 1
            });
        }
    }
}