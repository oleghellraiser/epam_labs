﻿namespace TicketReseller.Data.Models
{
    public enum OrderStatus
    {
        Accepted,
        Declined,
        InProgress
    };
}
