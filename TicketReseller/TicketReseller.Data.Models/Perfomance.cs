﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketReseller.Data.Models
{
    public class Perfomance : BaseEntity
    {
        [Required(ErrorMessage = "Please enter Perfomance name!")]
        [Display(Name = "Name : ")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Image : ")]
        public string Image { get; set; }

        [Required(ErrorMessage = "Please enter Perfomance date!")]
        [Display(Name = "Date : ")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Please enter Venue!")]
        [Display(Name = "Venue : ")]
        public int? VenueId { get; set; }
        public Venue Venue { get; set; }

        [Required(ErrorMessage = "Please enter Perfomance description!")]
        [Display(Name = "Description: ")]
        public string Description { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
    }
}
