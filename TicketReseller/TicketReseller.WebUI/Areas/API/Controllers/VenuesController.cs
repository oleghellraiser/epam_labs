﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using AutoMapper;
using TicketReseller.WebUI.Areas.API.ModelDTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.API.Controllers
{
    [Area("API")]
    [Route("[Area]/[controller]")]
    public class VenuesController : Controller
    {
        private UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public VenuesController(UnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get([FromHeader]string Accept)
        {
            var venues = _unitOfWork.VenueRepository.Get();
            if (Accept.Contains("application/csv"))
            {
                return Ok(_mapper.Map<List<VenueDTOforCSV>>(venues));
            }
            else
            {
                return Ok(_mapper.Map<List<VenueDTO>>(venues));
            }
        }
    }
}
