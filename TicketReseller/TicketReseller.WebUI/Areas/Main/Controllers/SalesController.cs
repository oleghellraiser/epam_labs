﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using Microsoft.AspNetCore.Authorization;
using TicketReseller.Data.Models;
using Microsoft.EntityFrameworkCore;
using TicketReseller.WebUI.ViewModels;
using Microsoft.Extensions.Localization;

namespace TicketReseller.WebUI.Controllers
{
    [Authorize]
    [Area("Main")]
    public class SalesController : Controller
    {
        private UnitOfWork _unitOfWork;
        private readonly IStringLocalizer<SalesController> localizer;

        public SalesController(IStringLocalizer<SalesController> localizer, UnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this.localizer = localizer;
        }
        // GET: /<controller>/
        public IActionResult MyOrders(OrderStatus filterParam)
        {
            var user = _unitOfWork.UserRepository.GetUserByEmail(HttpContext.User.Identity.Name);
            var orders = _unitOfWork.OrderRepository.Get(i => (i.UserId == user.Id) && (i.StatusId == filterParam));

            ViewBag.filterParam = filterParam;
            return View(orders);
        }

        // GET: /<controller>/
        public IActionResult MyTickets(TicketStatus filterParam)
        {
            var user = _unitOfWork.UserRepository.GetUserByEmail(HttpContext.User.Identity.Name);
            var tickets = _unitOfWork.TicketRepository.Get(t => (t.UserId == user.Id) && (t.StatusId == filterParam));
            List<TakenTicketViewModel> ticketListView = new List<TakenTicketViewModel>();
            List<Order> orders;

            switch (filterParam)
            {
                case TicketStatus.Selling:
                    tickets.ForEach(t => ticketListView.Add(new TakenTicketViewModel("", t)));

                    break;

                case TicketStatus.WaitingConfirmation:
                case TicketStatus.Sold:
                    orders = _unitOfWork.OrderRepository.Get(o => tickets.FirstOrDefault(t => t.Id == o.TicketId) != null);

                    orders.ForEach(o => ticketListView.Add(
                        new TakenTicketViewModel(o.User.FirstName + " " + o.User.LastName + " (" + o.User.Email + ")", o.Ticket)));

                    break;

                default:
                    tickets.ForEach(t => ticketListView.Add(new TakenTicketViewModel("", t)));

                    break;
            }
            ViewBag.filterParam = filterParam;
            return View(ticketListView);
        }

        public IActionResult AddingTicket(int id)
        {
            var user = _unitOfWork.UserRepository.GetUserByEmail(HttpContext.User.Identity.Name);
            if (user.FirstName == null || user.LastName == null || user.PhoneNumber == null || user.Address == null)
            {
                TempData["ErrorMessage"] = localizer["Please fill in information on User Info."];
                return RedirectToAction("Details", "Home", new { id = id });
            }
            var perfomance = _unitOfWork.PerfomanceRepository.GetByID(id);

            var ticket = new Ticket();
            ticket.PerfomanceId = id;
            ticket.Perfomance = perfomance;
            ticket.UserId = user.Id;
            ticket.User = user;

            return View(ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddingTicket(Ticket ticket)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ticket.StatusId = TicketStatus.Selling;

                    _unitOfWork.TicketRepository.Insert(ticket);
                    _unitOfWork.Save();
                    return RedirectToAction("Details", "Home", new { id = ticket.PerfomanceId });
                }
            }
            catch (DbUpdateException /* dex */)
            {
                ModelState.AddModelError(string.Empty, localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."]);
            }
            return View(ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MakingOrder(int id)
        {
            var ticket = _unitOfWork.TicketRepository.GetByID(id);
            try
            {
                if (ticket != null)
                {
                    var user = _unitOfWork.UserRepository.GetUserByEmail(HttpContext.User.Identity.Name);
                    if (user.FirstName == null || user.LastName == null || user.PhoneNumber == null || user.Address == null)
                    {
                        TempData["ErrorMessage"] = localizer["Please fill in information on User Info."];
                        return RedirectToAction("Details", "Home", new { id = ticket.PerfomanceId });
                    }

                    var order = new Order();
                    order.TicketId = id;
                    order.UserId = user.Id;
                    order.StatusId = OrderStatus.InProgress;

                    ticket.StatusId = TicketStatus.WaitingConfirmation;

                    _unitOfWork.OrderRepository.Insert(order);
                    _unitOfWork.TicketRepository.Update(ticket);

                    _unitOfWork.Save();
                    return RedirectToAction("Details", "Home", new { id = ticket.PerfomanceId });
                }
            }
            catch (DbUpdateException /* dex */)
            {
                TempData["ErrorMessage"] = localizer["Unable to save changes. Try again, and if the problem persists contact your system administrator."];
            }
            return RedirectToAction("Details", "Home", new { id = ticket.PerfomanceId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveOrder(int id)
        {
            try
            {
                var order = _unitOfWork.OrderRepository.GetByID(id);

                if ((order == null) || (order.StatusId != OrderStatus.InProgress))
                {
                    return RedirectToAction("MyOrders", "Sales", new { filterParam = OrderStatus.InProgress });
                }

                order.Ticket.StatusId = TicketStatus.Selling;

                _unitOfWork.TicketRepository.Update(order.Ticket);
                _unitOfWork.OrderRepository.Delete(id);

                _unitOfWork.Save();
            }
            catch (DbUpdateException /* dex */)
            {
                TempData["ErrorMessageOrders"] =
                    localizer["Delete failed. Try again, and if the problem persists " +
                    "see your system administrator."];
            }
            return RedirectToAction("MyOrders", "Sales", new { filterParam = OrderStatus.InProgress });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmOrder(int index, string trackNumber)
        {
            var order = _unitOfWork.OrderRepository.Get(o => (o.TicketId == index) && (o.StatusId == OrderStatus.InProgress))
                .SingleOrDefault();

            order.TrackNum = trackNumber;
            order.StatusId = OrderStatus.Accepted;
            order.Ticket.StatusId = TicketStatus.Sold;

            _unitOfWork.OrderRepository.Update(order);
            _unitOfWork.TicketRepository.Update(order.Ticket);

            _unitOfWork.Save();

            return RedirectToAction("MyTickets", "Sales", new { filterParam = TicketStatus.WaitingConfirmation });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RejectOrder(int index, string rejectionReason)
        {
            var order = _unitOfWork.OrderRepository.Get(o => (o.TicketId == index) && (o.StatusId == OrderStatus.InProgress))
                .SingleOrDefault();

            order.RejectionReason = rejectionReason;
            order.StatusId = OrderStatus.Declined;
            order.Ticket.StatusId = TicketStatus.Selling;

            _unitOfWork.OrderRepository.Update(order);
            _unitOfWork.TicketRepository.Update(order.Ticket);

            _unitOfWork.Save();
            return RedirectToAction("MyTickets", "Sales", new { filterParam = TicketStatus.WaitingConfirmation });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveTicket(int id)
        {
            try
            {
                var ticket = _unitOfWork.TicketRepository.GetByID(id);

                if ((ticket == null) || (ticket.StatusId != TicketStatus.Selling))
                {
                    return RedirectToAction("MyTickets", "Sales", new { filterParam = TicketStatus.Selling });
                }

                _unitOfWork.TicketRepository.Delete(id);

                _unitOfWork.Save();
            }
            catch (DbUpdateException /* dex */)
            {
                TempData["ErrorMessageTickets"] =
                    localizer["Delete failed. Try again, and if the problem persists " +
                    "see your system administrator."];
            }
            return RedirectToAction("MyTickets", "Sales", new { filterParam = TicketStatus.Selling });
        }
    }
}
