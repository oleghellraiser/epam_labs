﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class PerfomanceRepository : GenericRepository<Perfomance>
    {
        public PerfomanceRepository(DataContext context) : base(context)
        {
        }

        public override List<Perfomance> Get(Expression<Func<Perfomance, bool>> filter = null, Func<IQueryable<Perfomance>,
            IOrderedQueryable<Perfomance>> orderBy = null, Expression<Func<Perfomance, object>>[] includeProperties = null)
        {
            var perfomances = _context.Set<Perfomance>()
                .Include(p => p.Tickets)
                .Include(p => p.Venue)
                .ThenInclude(v => v.City)
                .AsQueryable();

            if (filter != null)
            {
                perfomances = perfomances.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(perfomances).ToList();
            }
            return perfomances.ToList();
        }

        public List<Perfomance> Get(DateTime fromDate, DateTime toDate,
            List<int> cityIndexes, List<int> venueIndexes,
            string orderName = "DATE", string partialName = "")
        {
            var perfomances = _context.Set<Perfomance>()
                .Include(p => p.Tickets)
                .Include(p => p.Venue)
                .ThenInclude(v => v.City)
                .AsQueryable();

            Func<IQueryable<Perfomance>, IOrderedQueryable<Perfomance>> orderBy =
                p => p.OrderBy(i => i.Date);

            if (!string.IsNullOrEmpty(orderName) && orderName.ToUpper() != "DATE")
                orderBy = p => p.OrderBy(i => i.Name);

            perfomances = orderBy(perfomances);

            if (!string.IsNullOrEmpty(partialName) && partialName != null)
                perfomances = perfomances.Where(p => p.Name.ToLower().Contains(partialName.ToLower()));

            if (fromDate > DateTime.MinValue && perfomances != null)
                perfomances = perfomances.Where(p => fromDate <= p.Date);

            if (toDate > DateTime.MinValue && perfomances != null)
                perfomances = perfomances.Where(p => p.Date <= toDate);

            if (cityIndexes.Count != 0 && perfomances != null)
                perfomances = perfomances.Where(p => cityIndexes.Contains((int)p.Venue.CityId));

            if (venueIndexes.Count != 0 && perfomances != null)
                perfomances = perfomances.Where(p => venueIndexes.Contains(p.Venue.Id));

            return perfomances.ToList();
        }
    }
}
