﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReseller.Data.Models;
using TicketReseller.WebUI.Areas.Admin.ViewModels;
using TicketReseller.WebUI.Areas.API.ModelDTO;

namespace TicketReseller.WebUI.Infrastructure
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
            : this("MyProfile")
        {
        }

        protected AutoMapperProfileConfiguration(string profileName)
        : base(profileName)
        {
            CreateMap<User, UserViewModel>();
            CreateMap<Venue, VenueDTO>();

            CreateMap<Perfomance, PerfomanceDTO>()
                .ForMember(dto => dto.Date, opt => opt.MapFrom(entity => entity.Date.ToString("d", HandlingFormat.GetFormatDate())))
                .ForMember(dto => dto.Availible, opt => opt.MapFrom(entity => entity
                    .Tickets.Where(i => i.StatusId == TicketStatus.Selling).Count()));

            CreateMap<Perfomance, PerfomanceDTOforCSV>()
                .ForMember(dto => dto.Date, opt => opt.MapFrom(entity => entity.Date.ToString("d", HandlingFormat.GetFormatDate())))
                .ForMember(dto => dto.City, opt => opt.MapFrom(entity => entity.Venue.City.Name))
                .ForMember(dto => dto.Address, opt => opt.MapFrom(entity => entity.Venue.Address))
                .ForMember(dto => dto.Venue, opt => opt.MapFrom(entity => entity.Venue.Name))
                .ForMember(dto => dto.Availible, opt => opt.MapFrom(entity => entity
                    .Tickets.Where(i => i.StatusId == TicketStatus.Selling).Count()));

            CreateMap<Venue, VenueDTOforCSV>()
                .ForMember(dto => dto.City, opt => opt.MapFrom(entity => entity.City.Name));
        }
    }
}
