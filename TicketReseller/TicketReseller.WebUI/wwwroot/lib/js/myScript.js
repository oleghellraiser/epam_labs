﻿
$(document).ready(function () {

    var selectedolditem = localStorage.getItem('selectedolditem');

    if (selectedolditem != null) {
        $("#filterCities li a").parents('.dropdown').find('.dropdown-toggle').html(selectedolditem);
        localStorage.clear();
    }

    // Add smooth scrolling to all links in navbar + footer link
    $("footer a[href='#myPage']").on('click', function (event) {

        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 900, function () {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
        });
    });

    // Slide in elements on scroll
    $(window).scroll(function () {
        $(".slideanim").each(function () {
            var pos = $(this).offset().top;

            var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
                $(this).addClass("slide");
            }
        });
    });
    $('#submit-perfomance').on('click', function () {
        var imgSource = $("input.input-image[name ='uploadedFile']").val();
        if (imgSource) {
            $("input.input-image[type='hidden']").val(imgSource);
        }
    });
})
$('a[href="' + this.location.href.replace(this.location.origin, "") + '"]').parents('li,ul').addClass('active');

listItem = $('.list-group a');
listItem.each(function (index, item) {
    if (window.location.href.includes(item.href)) {
        $('.list-group a').removeClass('active');
        $(item).addClass('active');
    }
});

$("#filterCities li a").click(function () {
    var selText = $(this).text();

    localStorage.setItem("selectedolditem", selText);
});


$('.open-deletionDialog').click(function () {
    var input = $('.yes-delete');
    input.val($(this).data("index"));

    $.each($(event.target).data(), function (key, value) {
        if (value != null) {
            var element = document.getElementById(key)
            if (element) {
                element.textContent = value;
            }
        }
    });
});

$('#myModal').on('show.bs.modal', function () {
    $(this).find('.modal-dialog').css({
        'margin-top': function () {
            return (($(window).outerHeight() / 2) - ($(this).outerHeight() / 2));
        }
    });
});

$("input[type='datetime']").datepicker({
    format: "dd.mm.yy",
    weekStart: 1,
    startDate: "01/01/2000",
    forceParse: false,
    orientation: "bottom auto",
    daysOfWeekHighlighted: "0,6",
    autoclose: true,
    todayHighlight: true
});

var $myGroup = $('.myGroup');
$myGroup.on('show.bs.collapse', '.collapse', function () {
    $myGroup.find('.collapse').collapse('hide');
});