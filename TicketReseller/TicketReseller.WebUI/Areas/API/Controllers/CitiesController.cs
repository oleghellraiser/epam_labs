﻿using Microsoft.AspNetCore.Mvc;
using TicketReseller.Data.Context;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicketReseller.WebUI.Areas.API.Controllers
{
    [Area("API")]
    [Route("[Area]/[controller]")]
    public class CitiesController : Controller
    {
        private UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CitiesController(UnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get([FromHeader]string Accept)
        {
            return Ok(_unitOfWork.CityRepository.Get());
        }
    }
}
