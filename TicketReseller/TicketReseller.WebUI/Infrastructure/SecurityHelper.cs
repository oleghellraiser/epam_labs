﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace TicketReseller.WebUI.Infrastructure
{
    public static class SecurityHelper
    {
        public static string Hash(string s)
        {
            byte[] salt = new byte[128 / 8];
            salt = (Encoding.UTF8).GetBytes("crypting");

            string hash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: s,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return hash;
        }
    }
}
