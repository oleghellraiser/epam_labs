﻿namespace TicketReseller.WebUI.Infrastructure.FormattersAPI
{
    public class CsvFormatterOptions
    {
        public bool UseSingleLineHeaderInCsv { get; set; } = true;

        public string CsvDelimiter { get; set; } = ";";
    }
}
