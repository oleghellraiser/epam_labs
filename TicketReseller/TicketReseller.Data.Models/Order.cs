﻿namespace TicketReseller.Data.Models
{
    public class Order : BaseEntity
    {
        public int? TicketId { get; set; }
        public Ticket Ticket { get; set; }
        public OrderStatus StatusId { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public string TrackNum { get; set; }
        public string RejectionReason { get; set; }
    }
}
