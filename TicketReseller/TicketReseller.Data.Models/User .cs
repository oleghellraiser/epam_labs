﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicketReseller.Data.Models
{
    public class User : BaseEntity
    {
        [Required(ErrorMessage = "Input your Email.")]
        [RegularExpression(@".+@.+\..+", ErrorMessage = "Incorrect data")]
        [StringLength(25)]
        [Display(Name = "E-mail: ")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your first name!")]
        [RegularExpression(@"\w+", ErrorMessage = "Incorrect data")]
        [StringLength(25)]
        [Display(Name = "First name: ")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your last name!")]
        [RegularExpression(@"\w+", ErrorMessage = "Incorrect data")]
        [StringLength(25)]
        [Display(Name = "Last name: ")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Input your password.")]
        [Display(Name = "Password: ")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter your Address!")]
        [Display(Name = "Address: ")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please enter your phone!")]
        [RegularExpression(@"\+?\d+", ErrorMessage = "Incorrect data")]
        [StringLength(15)]
        [Display(Name = "Phone: ")]
        public string PhoneNumber { get; set; }

        public UserLocalization LocalizationId { get; set; }
        public UserRole RoleId { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
    }
}
