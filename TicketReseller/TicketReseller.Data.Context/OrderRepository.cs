﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class OrderRepository : GenericRepository<Order>
    {
        public OrderRepository(DataContext context) : base(context)
        {
        }

        public override List<Order> Get(Expression<Func<Order, bool>> filter = null, Func<IQueryable<Order>,
            IOrderedQueryable<Order>> orderBy = null, Expression<Func<Order, object>>[] includeProperties = null)
        {
            var orders = _context.Set<Order>()
                .Include(o => o.User)
                .Include(o => o.Ticket)
                .ThenInclude(t => t.User)
                .Include(t => t.Ticket.Perfomance)
                .AsQueryable();

            if (filter != null)
            {
                orders = orders.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(orders).ToList();
            }
            return orders.ToList();
        }

        public override Order GetByID(int id)
        {
            var order = _context.Set<Order>()
                .Include(t => t.Ticket)
                .ThenInclude(t => t.Perfomance)
                .FirstOrDefault(c => c.Id == id);
            return order;
        }
    }
}
