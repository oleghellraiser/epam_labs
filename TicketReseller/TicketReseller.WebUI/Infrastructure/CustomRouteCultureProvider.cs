﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TicketReseller.WebUI.Infrastructure
{
    public class CustomRouteCultureProvider : IRequestCultureProvider
    {
        private static RequestCulture _requestCulture;

        public CustomRouteCultureProvider(RequestCulture requestCulture)
        {
            _requestCulture = requestCulture;
        }

        public static string GetDefaultCulture
        {
            get
            {
                return CustomRouteCultureProvider._requestCulture.Culture.TwoLetterISOLanguageName;
            }
        }

        public Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            PathString url = httpContext.Request.Path;

            // Have any culture in URL
            if (url.ToString().Length <= 1)
            {
                // Set default Culture and UICulture
                return Task.FromResult<ProviderCultureResult>(
                    new ProviderCultureResult(
                        _requestCulture.Culture.TwoLetterISOLanguageName,
                        _requestCulture.UICulture.TwoLetterISOLanguageName));
            }

            string culture = httpContext.Request.Path.Value.Split('/').GetValue(1).ToString();

            // Test if the culture is properly formatted
            if (!Regex.IsMatch(culture, @"^[a-z]{2}(-[A-Z]{2})*$"))
            {
                // Set default Culture and UICulture
                return Task.FromResult<ProviderCultureResult>(
                    new ProviderCultureResult(
                        _requestCulture.Culture.TwoLetterISOLanguageName,
                        _requestCulture.UICulture.TwoLetterISOLanguageName));
            }

            // Set Culture and UICulture from route culture parameter
            return Task.FromResult<ProviderCultureResult>(
                new ProviderCultureResult(culture, culture));
        }
    }
}