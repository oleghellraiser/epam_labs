﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class VenueRepository : GenericRepository<Venue>
    {
        public VenueRepository(DataContext context) : base(context)
        {
        }

        public override List<Venue> Get(Expression<Func<Venue, bool>> filter = null, Func<IQueryable<Venue>,
            IOrderedQueryable<Venue>> orderBy = null, Expression<Func<Venue, object>>[] includeProperties = null)
        {
            var venues = _context.Set<Venue>()
                .Include(t => t.City)
                .AsQueryable();

            if (filter != null)
            {
                venues = venues.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(venues).ToList();
            }
            return venues.ToList();
        }

        public override Venue GetByID(int id)
        {
            var venue = _context.Set<Venue>()
                .Include(t => t.City)
                .FirstOrDefault(c => c.Id == id);
            return venue;
        }
    }
}
