﻿using System.Collections.Generic;
using TicketReseller.Data.Models;

namespace TicketReseller.WebUI.ViewModels
{

    public class PerfomanceListViewModel
    {
        public IEnumerable<Perfomance> Perfomances { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
