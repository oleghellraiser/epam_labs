﻿namespace TicketReseller.Data.Models
{
    public enum TicketStatus
    {
        Selling,
        WaitingConfirmation,
        Sold
    };
}
