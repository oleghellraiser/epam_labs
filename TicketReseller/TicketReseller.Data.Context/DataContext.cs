﻿using Microsoft.EntityFrameworkCore;
using TicketReseller.Data.Models;

namespace TicketReseller.Data.Context
{
    public class DataContext : DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Perfomance> Perfomances { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<User> Users { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
  
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>( b =>
            {
                b.HasOne(p => p.Ticket)
                    .WithMany()
                    .HasForeignKey(p => p.TicketId);

                b.HasOne(p => p.User)
                    .WithMany()
                    .HasForeignKey(p => p.UserId);
            });

            modelBuilder.Entity<Perfomance>(b =>
            {
                b.HasOne(p => p.Venue)
                    .WithMany(p => p.Perfomances)
                    .HasForeignKey(p => p.VenueId);
            });

            modelBuilder.Entity<Ticket>(b =>
            {
                b.HasOne(p => p.Perfomance)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(p => p.PerfomanceId);

                b.HasOne(p => p.User)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(p => p.UserId);
            });

            modelBuilder.Entity<Venue>(b =>
            {
                b.HasOne(p => p.City)
                    .WithMany()
                    .HasForeignKey(p => p.CityId);
            });

        }
    }
}
